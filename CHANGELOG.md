# CHANGELOG

<!--- next entry here -->

## 1.0.1
2020-01-28

### Fixes

- **ci:** Fixes git tag fetching. (e332a36a38f450c322dd112763691a3a735cdedc)

## 1.0.0
2020-01-28

### Features

- **ci:** Initial Commit. (3c016ef0f3471506a365c0a113b98a6119931f1c)

### Fixes

- **docker:** 1.0.0 BREAKING CHANGE:UPDATE (b403cc81c4b02064887eb211b492c696dd99fa76)
- **ci:** Update go-semrel-gitlab (v0.20.4 -> v0.21.1). (1948e67123814bb9c9fd8e3462339890caca2c6c)

## 1.0.0
2020-01-13

### Features

- **ci:** Initial Commit. (3c016ef0f3471506a365c0a113b98a6119931f1c)

### Fixes

- **docker:** 1.0.0 BREAKING CHANGE:UPDATE (b403cc81c4b02064887eb211b492c696dd99fa76)

## 1.0.0-beta.1
2020-01-13

### Features

- **ci:** Initial Commit. (3c016ef0f3471506a365c0a113b98a6119931f1c)

### Fixes

- **docker:** 1.0.0 BREAKING CHANGE:UPDATE (b403cc81c4b02064887eb211b492c696dd99fa76)

## 1.0.0-alpha.1
2020-01-13

### Features

- **ci:** Initial Commit. (3c016ef0f3471506a365c0a113b98a6119931f1c)

### Fixes

- **docker:** 1.0.0 BREAKING CHANGE:UPDATE (b403cc81c4b02064887eb211b492c696dd99fa76)